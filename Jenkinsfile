pipeline{
	agent none
	options{
		timeout(time: 1, unit: 'HOURS')
	}
	parameters{
		booleanParam(name: 'UNITTEST', defaultValue: true, description: 'Enable UnitTests ?')
		booleanParam(name: 'CODEANALYSIS', defaultValue: true, description: 'Enable CODE-ANALYSIS ?')
	}
	stages{
		stage('SCM Checkout'){
			agent { label 'agentci' }
			steps{
				git branch: 'master', url: 'https://gitlab.com/suryarallabandi/melodyapp.git'
			}
		}
		stage('PreCheck'){
			when{
				anyOf {
		           changeset "melody/**"
     			}
			}
			steps{
				script{
					env.BUILDME = "yes"
				}
			}
		}
		stage('Build Artifacts'){
			agent { label 'agentci' }
		   	when {environment name: 'BUILDME', value: 'yes'}
		   	steps { 
		    	script {
			    	if (params.UNITTEST) {
				  		unitstr = ""
					} 
					else {
				  		unitstr = "-Dmaven.test.skip=true"
					}
					echo "Building Jar Component ..."
						dir ("./melody") {
				   			sh "mvn clean package ${unitstr}"
						}
					 }
		   		}
		  	}
		}
		stage('Code Coverage'){
   			agent { label 'agentsec' }
   			when {
     			allOf {
		         expression { return params.CODEANALYSIS }
		         environment name: 'BUILDME', value: 'yes'
     			}
   			}
   			steps {
			    echo "Running Code Coverage ..."  
				dir ("./melody") {
					sh "mvn org.jacoco:jacoco-maven-plugin:0.5.5.201112152213:prepare-agent"
				}
   			}
  		}
  		stage('SonarQube Analysis'){
    		agent { label 'agentsec' }
    		when {environment name: 'BUILDME', value: 'yes'}
    		steps{
     			withSonarQubeEnv('SonarQube') {
	  				dir ("./melody") {
         				sh 'mvn sonar:sonar'
	  				}
     			} 
    		}
  		}
  		stage("Quality Gate"){
  			agent { label 'agentsec' } 
    		when {environment name: 'BUILDME', value: 'yes'}
    		steps{
	 			script {
	  				timeout(time: 10, unit: 'MINUTES') {
				        def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
				        if (qg.status != 'OK') {
				           error "Pipeline aborted due to quality gate failure: ${qg.status}"
				        }
      				}
	 			}
    		}
  		}
		stage('Stage Artifacts'){          
   			agent { label 'agentsec' }
   			when {environment name: 'BUILDME', value: 'yes'}
   			steps {          
    			script { 
				    /* Define the Artifactory Server details */
			        def server = Artifactory.server 'JFrog'
			        def uploadSpec = """{
			            "files": [{
			            "pattern": "melody/target/melody.war", 
			            "target": "Melody"                   
			            }]
			        }"""
			        /* Upload the war to  Artifactory repo */
			        server.upload(uploadSpec)
    			}
   			}
  		}
	}
}
